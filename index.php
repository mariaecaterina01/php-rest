<?php
require_once 'func.inc.php';

// name, age, skills
$data = array ();

if ($_GET ['format'] === 'json') {
	handleJsonFormat ( $data );
} else if ($_GET ['format'] === 'xml') {
	handleXmlFormat ( $data );
} else {
	handleStringFormat ( $data );
}
?>